The purpose of this project is to give those who want more practice and to excersise what they have been taught a place to do so. 

The Dannon philosophy is to do something you have an interest in and it will be fun, engaging, and the best learning experience. 

Ideally, we will have a brainstorming session that will allow us to come up with a common interest and project.

With that idea, we will plan out what it will look like. 

After we have a picture of what we want to create, we can pair program the pieces of work. We should have experience from the previous class project.


Ideas:
- a todo list that allows one to add and remove tasks
- a shopping list that allows one to add links
- a site that will house recipes
- a bug smashing game
